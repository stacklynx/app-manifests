resource "aws_db_instance" "default" {
  allocated_storage    = {{disk_size}}
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "{{instance_class}}"
  identifier           = "{{instance_identifier}}"
  name                 = "{{instance_name}}"
  username             = "{{db_username}}"
  skip_final_snapshot  = {{skip_final_snapshot}}
  password             = "{{db_password}}"
  parameter_group_name = "default.mysql5.7"
}